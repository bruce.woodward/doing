# Time tracking from the command line.

Answer the question, "What did I do and how long did I spending doing X"

# Logging that you are working on a task.

$ doing task1 a short description or explaination.

Later on ...

$ doing task1 something other part of this task.

Still later on ...

$ doing task1 just finishing up this task.

# Looking at you spent your time on.

## To all tasks for the day

$ did

## To see just "task1"

$ did task1

## To see specific tasks

$ did task1 task2

## To see tasks for yesterday

$ did --yesterday

## To see specific tasks for yesterday

$ did task1 --yesterday

## To see all tasks for today and yesterday

$ did --today --yesterday

# Supported time frames.

--today
--yesterday
--month
--from=DATE
--to=DATE
